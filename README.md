# CICD workflow

* push based, uses the kubernetes agent ci/cd tunnel (aka CICD workflow) https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_tunnel.html
* redeploys https://cicdworkflow.falcorocks.com
* manifest docker tag is replaced at deployment through envsubsts to use the CI_COMMIT_SHORT_SHA
